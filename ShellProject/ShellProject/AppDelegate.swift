//
//  AppDelegate.swift
//  ShellProject
//
//  Created by KingYoung on 2020/05/26.
//  Copyright © 2020 KingYoung. All rights reserved.
//

import UIKit
import Combine

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // 1
        let just = Just("Hello world!")

        // 2
        _ = just
        .sink(
        receiveCompletion: {
        print("Received completion", $0)
        },
        receiveValue: {
        print("Received value", $0)
        })
        
        _ = just
        .sink(
        receiveCompletion: {
        print("Received completion (another)", $0)
        },
        receiveValue: {
        print("Received value (another)", $0)
        })
    
//        let publisher = NotificationCenter.default.publisher(for: Notification.Name(rawValue: "hello"))
//
//        publisher.sink { _ in
//            print("收到了")
//        }
//
//        NotificationCenter.default.post(name: Notification.Name(rawValue: "hello"), object: nil)
//
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = HomeTabBarC()
        window?.makeKeyAndVisible()
        return true
    }


}

