//
//  HomeTabBarC.swift
//  ProjectDemo
//
//  Created by KingYoung on 2020/05/8.
//  Copyright © 2020 KingYoung. All rights reserved.
//

import UIKit
//import Audi_GSDK

private let selectItemKey = "selectItemKey"


class HomeTabBarC: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        
        setupAllChildViewControllers()
    }
    
    func setupAllChildViewControllers() {
//        let nav = UINavigationController(rootViewController: DemoTableVC())
//        addChild(nav)

    }
    
    func setup(childVC: UIViewController, normalImage: UIImage, selectedImage: UIImage) {
        
        childVC.tabBarItem.image = normalImage
        childVC.tabBarItem.selectedImage = selectedImage
        let nav = UINavigationController(rootViewController: childVC)
        addChild(nav)
        
    }
}
